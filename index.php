<!DOCTYPE html>
<html lang="th">
<head>
    <title>Login</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
 <body>
 <?php
    $oauth_url = "https://www1.reg.cmu.ac.th/reomi/oauth-app-regist";
    session_start();
    if(!isset($_SESSION["oauth"])) {
        echo '<meta http-equiv="refresh" content="0; url=https://www1.reg.cmu.ac.th/reg-platform/oauth?redirect_url='.$oauth_url.'" />';
    } else {
        $studentId = $_SESSION["oauth"]["student_id"];
        $itaccount = $_SESSION["oauth"]["itaccount"];
        $itaccountName = $_SESSION["oauth"]["itaccount_name"];
        $prefixTh = $_SESSION["oauth"]["prefix_th"];
        $prefixEn = $_SESSION["oauth"]["prefix_en"];
        $firstNameTh = $_SESSION["oauth"]["first_name_th"];
        $lastNameTh = $_SESSION["oauth"]["last_name_th"];
        $firstNameEn = $_SESSION["oauth"]["first_name_en"];
        $lastNameEn = $_SESSION["oauth"]["last_name_en"];
        $organizationCode = $_SESSION["oauth"]["organization_code"];
        $organizationNameTh = $_SESSION["oauth"]["organization_name_th"];
        $organizationNameEn = $_SESSION["oauth"]["organization_name_en"];
        $citizenId = $_SESSION["oauth"]["citizen_id"];
        $itaccountTypeId = $_SESSION["oauth"]["itaccount_type_id"];
        $itaccountTypeNameTh = $_SESSION["oauth"]["itaccount_type_name_th"];
        $itaccountTypeNameEn = $_SESSION["oauth"]["itaccount_type_name_en"];
        if(isset($studentId)) {
            echo "<div>I'm  student id $studentId</div><br>";
        } else {
            // echo "<div>$prefixEn $firstNameEn $lastNameEn</div><br>";
            // echo "<ion-pane>";
            // echo "<ion-header-bar class=\"bar-stable\">";
            // echo "<h1 class=\"title\">พิมพ์รหัส นศ. เพื่อทดสอบระบบ</h1>";
            // echo "</ion-header-bar>";
            // echo "<ion-content>";
            // echo "<input type='text' id='data' />";
            // echo "<button type=\"button\" onclick=\"openInAppBrowser()\">Open In App Browser</button>";
            // echo "<h1 id=\"output\"></h1>";
            // echo "</ion-content>";
            // echo "</ion-pane>";
            echo '<div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <form class="login100-form validate-form">
                    <span class="login100-form-title p-b-26">
                        ยินดีต้อนรับ <br>
                        $prefixEn $firstNameEn $lastNameEn
                    </span>
                    <span class="login100-form-title p-b-48">
                        <i class="zmdi zmdi-male zmdi-hc-spin"></i>
                    </span>

                    <div class="wrap-input100 validate-input" data-validate = "ต้องไม่ว่างสิ">
                        <input class="input100" type="text" name="std_id">
                        <span class="focus-input100" data-placeholder="กรอกรหัส นศ. ที่ต้องการทดสอบ"></span>
                    </div>

                    <div class="container-login100-form-btn">
                        <div class="wrap-login100-form-btn">
                            <div class="login100-form-bgbtn"></div>
                            <button class="login100-form-btn" onclick="openInAppBrowser()">
                                เข้าทดสอบระบบ
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>';

            echo "<script type=\"text/javascript\">";
            echo "function openInAppBrowser() {";
            echo "var data = document.getElementById('data').value;";
            echo "localStorage.setItem('data', data);";
            echo "alert('Done!');";
            echo "close();";
            echo "}";
            echo "</script>";
        }
        session_destroy();
    }
    ?>
    <!--===============================================================================================-->
    <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
    <script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
    <script src="js/main.js"></script>
 </body>
</html>